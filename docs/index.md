# Overview

The main objective of this Collabathon is to identify and generate consensus around the known problems with soil organic carbon and management data on grazing lands that is limiting the ability of modelers to accurately estimate and predict soil carbon sequestration on grazing lands. Over a period of nine months we will leverage the expertise of a variety of stakeholders – including but not limited to ranchers, researchers, ag tech providers, marketplaces and buyers – to co-design and document proposed approaches to address the gaps identified during the Collabathon. In addition, concept notes resulting from the Collabathon will provide a clear pathway for deploying and testing the identified solutions. 

## Problem Statement

The current lack of spatially explicit soil carbon data (including SOC stocks and change over time) across a variety of outcome indicators as they correlate to management practices is limiting the ability of modelers to accurately estimate SOC sequestration on grazing lands. Furthermore, the rates at which SOC changes occur under the different management practices in diverse climatic/ pedologic/ topographic conditions introduces additional variability. This gap in management and soils data over time also makes it difficult to understand where the highest potential for soil carbon sequestration is across the US.  Much of the data collected is considered “grey data" or not of research quality.  This lack of high resolution management and soil carbon data also inhibits improved calibration data sets which can be used to improve sample site selection tools, remote sensing machine learning models, and biogeochemical models. Existing internal work within USDA and work by Point Blue Conservation on the RangeC project  highlights this challenge and provides a template for future work.  

Addressing the management and soil carbon data gap is necessary to improve the confidence in modeling and enable multi-model ensemble approaches. Solving for the data gap will enable users in the future to cost-effectively and accurately estimate soil carbon sequestration potential on grazing lands.  Addressing the gap is also a prerequisite for a multi-model ensemble approach, and/or creating training data sets for emerging AI based learning models.

## Objectives 

The main objective of this project is to identify and generate consensus around the known problems with SOC and management data on grazing lands that is limiting the ability of modelers to accurately estimate and predict soil carbon sequestration on grazing lands, and then leverage the expertise of a variety of stakeholders (including but not limited to ranchers, researchers, ag tech providers, marketplaces and buyers) to co-design and document proposed approaches to address the gaps identified in the project.


### Goal 1: Outline needs and provide context for various use cases
We recognize that various stakeholders have different data requirements, and want to document what data use-types would be sufficient to incorporate into calculation for the Greenhouse Gas Protocol.  We thus specifically tackle this project from the perspective of quantitative modeling: what soils and management data is needed to accurately predictively model estimated soil carbon sequestration on grazing lands?


### Goal 2: Mapping and understanding data gaps
We suggest that there are seven known gaps in regards to soil carbon sequestration in grazing:

1. Limited long-term management and land use conversion (LUC) data related to outcomes 
2. Long term consistent baseline soil health and SOC data
3. Inconsistent data collection tools 
4. Inconsistent language across the industry
5. Insufficient incentives to collect high quality soils and management data with tools that do exist  
6. High variation of grazing management practices and a lack of standardization/consistency in grazing management terms and definitions
7. The correlation between SOC and additional co-benefits

This project will explore each of these areas, identifying what data sources do exist, what social, economic and technical gaps exist and also outline proposed approaches to address the gaps - with a focus on leveraging upcoming PCSC efforts. 

Additionally, this project will leave room for emergent issues not described above.  

### Goal 3: Envisioning Solutions and Systems Based Design

Utilizing a collaborative co-design process, we will provide a space for all relevant stakeholders to envision what an ideal system might look like, from a technical, social and economic perspective. 


- What field level tools will provide producer incentives for high quality data collection? What types of standards need to be developed? 
- What is the data service needed to support improvements in quantitative modeling, including multi-model ensemble approaches to predictive analytics? 
- What is the ecosystem needed to make data accessible, usable for farm and ranch management but also improve the accuracy of scientific models?  
- Prioritization based on efficiency of time and cost.


You can find the full concept note [here](assets/GLCDI Discovery Phase Concept Note.pdf). 
