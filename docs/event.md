# Events

## Kickoff 

**Purpose**


The kickoff call for the Grazing Lands Carbon Data Initiative was held on Monday February 12th, 2024. The call was an opportunity for all stakeholders to learn more about the initiative including history and governance, as well as better understand what the next year will hold. We held a 20 minute Q&A in order to answer any questions. The meeting recording and presentation are linked below.


[First All Hands Recording](assets/GMT20240212-210146_Recording_1920x1080.mp4)

[//]: # (This is the deck embed)

[Kickoff All Hands Deck](assets/AH_1_deck.pdf)


## Second All Hands

**Purpose**


The second All Hands call followed the first round of Working Group sessions and was held on March 19th, 2024. During the call we reviewed the purpose of the initiative and findings from the first round of Working Group sessions. We broke out into discussion groups to discuss three topics: monitoring, modeling, and a calibration dataset. The meeting recording and presentation are linked below. 

[Second All Hands Recording](assets/video1458096350.mp4)

[//]: # (This is the deck embed)

[Second All Hands Deck](assets/Grazing Lands Carbon Data Initiative_ Discovery Phase. 2nd All Hands - March 19th, 2024.pdf)


## Third All Hands

**Purpose**


The third All Hands call followed the second round of Working Group sessions and was held on April 18th, 2024. During the call we reviewed the purpose of the initiative and the synthesis from the working group sessions, exploring key themes and tensions that arose. We broke out into pairs to discuss our reactions and thoughts about the findings to date. The meeting recording and presentation are linked below. 


[Third All Hands Recording](https://us06web.zoom.us/clips/share/Y95L2AArvwSALm44cj-g4ie7qQIWW-fOTaWd2-9ZHwgGtVosuJgvV4NFG5P_017gtuRGVg.EAZsksWYwZOmsKio)

[//]: # (This is the deck embed)

[Third All Hands Deck](assets/Grazing Lands Carbon Data Initiative_ Discovery Phase. 3rd All Hands- April 18th, 2024.pdf)