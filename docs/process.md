# Process

## Timeline

[//]: # (This is the image embed)

![Timeline](assets/timeline.png)

### Governing Structure
We structured collabathon engagement to include a steering committee composed of representative leaders from each of the core parts of the supply system. This included working groups representing producers and producer organizations, technical service providers supporting producers and companies, conservation organizations, research and academic organizations, Ag Tech companies, and brands who sell the finished goods to consumers.

A larger group of stakeholders were selected to review and provide critical input on the outputs and thinking from the working groups through All Hands sessions and later, document review. An additional layer draws from a broader set of stakeholders who would be appropriate to be informed about findings and progress, provide targeted input, and have some vested interest in the outcome. This results in an iterative filtering process as thinking and resources are progressively pushed out to the edges and returned to the center. 

### Engagement Process

In December 2023 the Steering Committee convened during the Sustainable Agriculture Summit in Charlotte, NC to review key documents, confirm the approach to engagement, and discuss the key issues and opportunities being addressed by the initiative. January 2024 allowed time to prepare materials for engaging the wider group of working group participants, reviewers, and stakeholders and ready them for the Kick Off All Hands call in February. 

Following the kick off call, one round of sessions with each working group over two weeks culminated in our second All Hands call where we reviewed preliminary findings with the wider stakeholder community. A second, two-week, round of working group sessions was followed by a synthesis of the two working group sessions. 

The Steering Committee convened in May in Chicago to review the detailed synthesis and accompanying research that would inform the concept notes, the next round of working group sessions, and help to answer the key questions the initiative is intending to answer in this discovery phase. 

During the final round of working group sessions, we will build a deeper understanding of the data gaps and challenges while simultanesouly developing funded concept notes with working group participants and material reviewers. 
