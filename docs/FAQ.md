# Frequently Asked Questions

## End Product and Goals

**What is the end product of the Grazing Data Service Collabathon? Is it a literature review, a gap analysis across sectors?**


- The end product we aim to develop consists of two main sets of documents. The first outlines the specifications for a Grazing Data Service, drawing on the BOLTS framework (Business, Organizational, Legal, Technical, and Social governance). This includes what is necessary to develop an accessible calibration data set for soil organic carbon across the United States for grazing lands, detailing the required fees, governance implications, and technical and legal requirements.
- The second set of documents will propose methods for testing this data service, dataset, and governance structure.
- This Collabathon serves as a discovery process to evaluate the extent of progress that can be achieved. Given the complexity of these tasks and processes, the collaborative, pre-competitive environment aims to ensure that all participants derive more value than would be possible if each organization or institution worked independently. Our goal is to optimally balance your time and effort with the generation of greater value.

## Starting Points and Collaborative Efforts

**What are some of the first steps in this process?**


- The initial steps include identifying and emphasizing what is already known and happening in the industry. We'll look at the lessons that have already been learned to start from a more advanced position together, rather than beginning from a point that has already been explored.

**Is one of the outcomes going to be a producer agreement / data sharing template?**


- That is already an active workstream with OpenTEAM, including the development of the Oath of Care, Data Hosting and Storage, and Data Bill of Rights documentation (see more information [here](https://openteam-agreements.community/)). So that’s not the primary focus of this Collabathon but is certainly informed by it.

**Is the primary goal the creation of the dataset or the creation of a set of protocols and practices, or both?**


- The focus is on developing the conditions necessary for the dataset. This includes determining what is needed to share, aggregate, and trust a dataset at a national level for use by various stakeholders. It also involves agreeing on a set of standards that will enable collective progress in constructing the dataset as a public good, ensuring it is publicly accessible and referenceable.

## Alignment with National Efforts

**How closely is this initiative aligned with the [National Greenhouse Gas Soil Carbon Monitoring Plan](https://www.nrcs.usda.gov/news/biden-harris-administration-announces-new-investments-to-improve-measurement-monitoring), which intends to launch a $300 million effort this quarter to test and enhance these data streams?**


- We are very connected with USDA on data standards and the IRA Data Infrastructure. In that announcement, grazing data is the area that the USDA has the least data and lowest quality data so that’s something we’re speaking a lot about on the data infrastructure side through our cooperative agreements with the USDA on the [Conservation Evaluation Monitoring Activities (CEMAs)](https://www.nrcs.usda.gov/programs-initiatives/eqip-environmental-quality-incentives/eqip-cpas-dias-and-cemas) and incorporating how we move through existing data structures and collection through the [National Dynamic Soils Database](https://www.nrcs.usda.gov/conservation-basics/natural-resource-concerns/soil/soil-survey), and how we also create a version that we can use externally to the USDA and how we share that public and private data. In summary, very coordinated efforts and intending for increased USDA participation as we move toward creating the calibration dataset and also coordinating with other [PCSC projects](https://www.usda.gov/climate-solutions/climate-smart-commodities) to harmonize data that would otherwise not be aggregatable. 

## Monitoring and Measurement Strategies

**How are you planning to monitor and measure these outcomes and indicators in a cost-effective way?**


- Addressing this question is central to the Collabathon's purpose. There exists a variety of methods for collecting data on soil and management practices. Our goal is to uncover all the challenges stakeholders have encountered or are currently facing in gathering this data. While we don't have all the answers just yet, our collective ambition through this Collabathon is to better comprehend these issues over the next 6-8 months.

## Data Types and Security

**What are the types of data you’re looking for?**


- Identifying the key indicators and data types currently available and those missing, which are essential for the Calibration Dataset, is crucial. Longterm, this process aims to release a minimum viable dataset with this group. We're looking to establish a common core or trunk from which to build.

**How will this Collabathon address data security, privacy, and sovereignty? Farmers may not be open to releasing this data.**


- This issue is at the heart of the Collabathon. A key focus is on understanding social and legal hurdles and establishing suitable data use agreements. The aim of OpenTEAM and the PCSC is to create an [Agricultural Data Wallet](https://drive.google.com/file/d/1fQp2vUCLeUqJVo0OsAYQb0ppbhj1DUFj/view) (see Ag Data Wallet UX Info Session [here](https://www.youtube.com/watch?v=bzfXixd1ZtE)) that grants farmers and ranchers control over their data, ensuring that the purposes of data sharing are enforceable. This approach forms the basis of OpenTEAM. The next step is building a trusted data system. In this Collabathon, the Calibration Dataset's quality will improve if data is shared with confidence and trust. Thus, the initiative prioritizes agreements and trust over technology, ensuring the maintenance of these principles.

**Is the dataset you're seeking one that tracks changes in soil organic carbon over time or examines the relationship between current management practices and carbon stocks?**


- The Collabathon aims to explore these questions and identify gaps in creating these datasets. It starts with figuring out the basic components needed to answer both questions. The Calibration Dataset is designed to be versatile over time. The focus is more on setting up a system to interpret the data meaningfully rather than on the specific conclusions that can be drawn from the data. These questions are what drive the Collabathon.


Should you have additional questions, you may submit them [here](https://forms.gle/bwaM15LHMAMTZGPW9).