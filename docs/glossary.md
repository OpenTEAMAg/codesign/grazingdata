# Glossary of Terms and Definitions


## Adaptive Management Planning
Adaptive Management is a process of continual improvement by adjusting actions taken to
achieve a goal or objective based on high-frequency observations and data-driven analysis rather
than by expert opinion, best practice recipe, or prescription. It requires a high level of system
understanding and observation, analysis, and communications feedback.


## Ag Data Wallet
An ag data wallet would provide secure storage and transactions of important data, populated by
producers or trusted advisors, under the control of the producer. While the word “wallet” evokes
both a place where important documents are kept, and something that is under an individual’s
control, an ag data wallet can go beyond that by providing mechanisms for individuals to safely
exchange data and giving opportunities for financial compensation for ecosystem services. This
system will give complete control of data back to producers while also maximizing the value of
that data through efficient, secure and cost effective data management at the producer's
discretion. The technology enables farm-level data portability and interoperability across
systems, using the MMRV of on-farm outcomes to support management and decision making
and increase access to various opportunities. This system further enables security, trust and data
sovereignty.


## Cadaster
An official register showing details of ownership, boundaries, and value of real property in a
district, made for taxation purposes. In terms of this narrative, the functionality of a cadaster is
similar to that of an environmental claims clearinghouse.


## CARE Data Principles
The CARE Principles for Indigenous Data Governance were created to advance the legal
principles underlying collective and individual data rights in the context of the United Nations
Declaration on the Rights of Indigenous Peoples (UNDRIP). CARE is an acronym that stands for
collective benefit, authority to control, responsibility, ethics.

While CARE can be considered part of the open data movement, it aims to build on other
standards such as FAIR (findable, accessible, interoperable, reusable) by considering power
differentials and historical contexts. The CARE Principles for Indigenous Data Governance are
“people and purpose-oriented, reflecting the crucial role of data in advancing Indigenous
innovation and self-determination.”


## Collabathon
This was a compound word created by combining collaboration with marathon. Collabathons are
a sustained collaboration effort with short sprints in service of long range shared goals
implemented by the OpenTEAM community to accomplish goals in key work streams. Each
Collabathon session has a defined goal, outcome, and proposed output shaped by a community
co-hosts. They may take anywhere from three to eight weeks (or even longer) to complete with
likely a weekly cadence of hour-long meetings to keep the momentum going.


## Common Onboarding
Common Onboarding is the process of entering the minimum amount of data for producers to be
able to benefit from the tools and community, and providing the minimum level of technical
skills to access the tools using the Common Onboarding Form. This form will allow producers to
enter the minimum amount of data required of them to benefit from different opportunities
through a shared question set, accessing multiple tools, opportunities, and benefits in agriculture.
This can be compared to the common application for high school students applying to higher
education institutions.


## Community-Driven Protocols
Protocols that are developed and maintained by a community of users and developers, rather than
a single entity. They are designed to be open-source and decentralized, allowing for greater
collaboration and innovation.


## CSA Connector
The CSA Connector is a tool that will connect all of the relevant participants in the emerging
climate smart commodity marketplace—buyers, producers, technical service providers, and
certifiers–with each other and with the information needed to support transactions among them.
Those using the connector will ultimately have easy access to a wide range of resources,
including environmental claims registries, calibration data sets, and other information libraries.


## Data Portability
The ability of an individual or organization to obtain and 'move' their data from one place,
platform, etc. to another.


## Data Sovereignty
Addresses who has control over, ownership, and manages of data or databases and under what
conditions (e.g., laws, agreements, etc.). This term is used differently by different groups and
lacks a universal definition, data sovereignty is usually referenced in the context of an
individual’s ability to fully create and control their credentials, identity, and related information
about themselves and their work. In the context of this narrative, data sovereignty ensures that
the individual or community about whom data is collected has knowledge of and meaningful
consent over how that information is used and shared by others by providing these individuals
and communities with the tools and resources to control, interpret, and act on their own data.


## Decision Support System
The terms Decision Support Tools (DST), or Decision Support Systems (DSS), refer to a wide
range of computer-based tools (simulation models, and/or techniques and methods) developed to
support decision analysis and participatory processes. A DSS consists of a database and different tools coupled with models, and it is provided with a dedicated interface in order to be directly and more easily accessible by non-specialists (e.g.that is, decision- makers). DSS have specific simulation and prediction capabilities and but are also used as a vehicle of communication, training, and experimentation. Principally, DSS can facilitate dialogue and exchange of information, thus providing insights to non-experts and supporting them in the exploration of management and policy options.


## Digital Certification
Certifications can be used to demonstrate green provenance, net-zero and biosecurity claims to
enable value creation and market access across supply chains. Most certifications require the
creation of a written plan, recordkeeping, and on-site inspections to verify compliance.
Managing records and written plans for certification digitally can streamline the recordkeeping process for producers, allowing them to use data across a range of certifications and export data in the format needed for any particular certification. Digital and technical infrastructure can enable secure exchange, management, and organization of data across a range of certification
needs, including organic, grass-fed, regenerative food safety, animal welfare, etc.


## Digital Certification Standards
Certifications can be used to demonstrate green provenance, net-zero and biosecurity claims to
enable value creation and market access across supply chains. Standards for certifications are to
be met by producers to achieve a particular certification. In this case, the data necessary to meet those standards is tracked digitally.


## Ecosystem Service Markets
Ecosystem services are the many and varied benefits that humans freely gain from the natural
environment and from properly-functioning ecosystems such as air and water quality, habitat,
esthetics, and recreation. A marketplace quantifies and creates markets based on the change over
time in those services.


## Electronic Authorization (E-Auth)
The process of establishing confidence in user identities electronically presented to an
information system. An example of electric authorization could include verifying the identity of a computer system user.


## Environmental Asset Claims
Environmental assets are defined as naturally occurring living and non-living entities of the
Earth, together comprising the bio-physical environment, that jointly deliver ecosystem services
to the benefit of current and future generation. An environmental asset claim refers to the process
of an individual submitting documentation that demonstrates environmental benefit as it relates
to that environmental asset, ensuring proper MMRV using tools acceptable to various
methodologies.


## Environmental Claims Clearinghouse
A clearinghouse of environmental claims enables the flexible development of new and diverse
environmental claim assets classes while providing a trusted methodology for claim identification and assurance of uniqueness. An ECC enables claims searches by boundary, claimant, duration and type and a common format to enable registered claims to avoid conflicts related to additionality or double counting.


## Environmental Product Declarations (EPD)
An Environmental Product Declaration (EPD) is a comprehensive, internationally harmonized report created by a product manufacturer that documents the ways in which a product, throughout its lifecycle, affects the environment. This report quantifies environmental information on the life cycle of that product to enable comparisons between products fulfilling the same function.


## Ex-Ante Power Analysis
The calculations used to estimate the smallest sample size needed for an experiment or research
question, given a required significance level, statistical power, and effect size. This ensures
adequate sampling densities and that data collected has sufficient power to detect changes over
time.


## FAIR Data Principles
FAIR data are data that follow the principles of findability, accessibility, interoperability, and
reusability. The acronym and principles were defined in a March 2016 paper in the journal
Scientific Data by a consortium of scientists and organizations. The FAIR principles emphasize
machine-actionability (i.e.that is, the capacity of computational systems to find, access, interoperate, and reuse data with none or minimal or no human intervention), because humans
increasingly rely on computational support to deal with data as a result of the increase in volume,
complexity, and creation speed of data.


## Fidelity
In the context of this narrative, we are referring to fidelity of observed greenhouse gas benefits
over time. This refers to a system of measuring and analyzing the degree to which MMRV protocols are implemented as intended.


## Generation of Collective Funding (GCF)
GCF is a process of collecting funds from multiple sources to finance a project or venture. It
involves pooling resources from different sources, such as individuals, businesses, and other
organizations, to create a larger fund for a specific purpose.


## High Fidelity Digital Provenance
Observations from multiple sources can be combined with user generated data to provide high
fidelity digital provenance to both environmental claims and practice completion.


## Historically Underserved (HU) Producers
Some groups of people are identified in Farm Bill legislation and in USDA policy as being
Historically Underserved (HU). Members of these groups have been historically underserved by,
or subject to discrimination in, Federal policies and programs. Four groups are defined by USDA
as “Historically Underserved,” including farmers or ranchers who are: Beginning; Socially
Disadvantaged; Veterans; and Limited Resource.


## Interoperability
Interoperability is a characteristic of a product or system whose interfaces are completely
understood to work with other products or systems, at present or in the future, in implementation
or access, and without any restrictions. While the term was initially defined for information
technology or systems engineering services to allow for information exchange, a broader definition takes into account social, political, and organizational factors that impact system-to-system performance.


## Interoperable Claims Standards
Create technical infrastructure to establish shared standards and protocols for making a claim,
such as an environmental product declaration or environmental asset claim, that is interoperable
and compatible with different claiming systems, certifiers, agencies, etc.


## Measuring, Monitoring, Reporting, and Verification (MMRV)
Data tracking of how implemented changes on a farm or ranch can impact soil carbon levels, greenhouse gas emissions, etc. Basically after collecting baseline data from a farm, additional tracking can be used to measure change over time, verifying benefits to the ecosystem in mitigating climate change.


## National Calibration Dataset
The National Calibration Dataset will be an important resource for ensuring that measurements
made by different instruments are consistent and reliable. It contains a set of data points that have
been carefully collected and tested to ensure accuracy, and these points serve as a benchmark for
all instruments to be compared against. This dataset is used by scientists, engineers, and other
professionals to make sure that the measurements they take are accurate and reliable.


## Open Source
Open source is a publicly accessible software design that can be modified and shared by multiple
users. This allows the source code to be inspected and enhanced by anyone. Because open source uses multiple collaborators, it allows for more control, increased security and stability, additional
training opportunities, and the foundation of communities centered around software design.


## Participant Funding Pool
[in development]


## Post-Farmgate Data
Data that is measured, collected, and monitored after agricultural products leave the farm. This is
different from farm management data which is collected on-farm, but may include carbon emissions resulting from how far products may have traveled or even transaction information from farther down the supply chain.


## Pre-Competitive Approach
Pre-competitive refers to a collective approach to bring together a diverse group of stakeholders
from across the agricultural industry to create new technologies and solutions that benefit this shared industry and the larger food system. Diverse efforts can be streamlined into solving
systematic problems using multiple industry perspectives. Using such an approach will allow for
more rapid advancements in our shared understanding of food systems, agroecosystems, climate science, and relationships between soil health, and human health.


## Provenance
Origin and ownership of observed greenhouse gas benefits.


## Public Land Library
Public Land Library is a publicly curated knowledge commons comprising a curated set of environmental, geological, and human history records of land that is documented and accessible. Just as documents and archives can be contributed to like a public library, its documents and archives can be contributed to, and they are searchable like a registry of deeds, while also protecting sensitive locations or data that might threaten food sovereignty.


## Resolution
Data Resolution is the least detectable difference in a measurement, record, or observation.


## Technical Service Provider
Technical service providers (TSPs) offer planning, design, and implementation services to agricultural producers such as farmers, ranchers, and private forest landowners on behalf of the Natural Resources Conservation Service (NRCS). This assistance helps improve the producer's operation.


## Third Party Digital Verification
The process of having an independent third party verify user's information and data to ensure
accuracy or transparency. In this case, Technical Service Providers will act as a third party to
verify environmental impacts of newly implemented, climate-smart practices on farms.


## Traceable Transaction Handling
Tracking of climate-smart products and their raw materials/components throughout the supply chain using digital market profile and storytelling kits which will also provide both meaning and
measurement of impact for supporting market development.